#ifndef FIZZBUZZ_H
#define FIZZBUZZ_H

#include <iostream>
using namespace std;

class FizzBuzz
{
public:
    int i;
    int iDivisorType;
    int iExecutionType;
    bool iRUN = true;

    FizzBuzz();
    ~FizzBuzz();
    void printFizz();
    void printBuzz();
    void printFizzBuzz();
    void printInt();
    void printError(string);
    void printLogic();

    void checkForThreeDivisor();
    void checkForFiveDivisor();
    void checkForFifteenDivisor();
    void checkEverything();
    void isJustNumber();

    void executeAutomated();
    void executeManually();

    void set_iExecutionType(int input) { iExecutionType = input; }
    int get_iExecutionType() { return iExecutionType; }
    bool get_iRUN() { return iRUN; }
};

#endif // FIZZBUZZ_H
