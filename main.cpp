#include <iostream>
#include "fizzbuzz.h"

using namespace std;

int main()
{

    FizzBuzz buzzer;

    bool RUN = buzzer.get_iRUN();
    int input;

    while (RUN == true)
    {

        std::cout << "Programm: FIZZBUZZ checker" << std::endl;
        std::cout << std::endl;
        std::cout << "Check this, multiples of 3 are replaced by Fizz" << std::endl;
        std::cout << "and... multiples of 5 are replaced by Buzz" << std::endl;
        std::cout << "and... multiples of 15 are replaced by FizzBuzz" << std::endl;
        std::cout << std::endl;

        std::cout << "Enter '1' to view the complete FizzBuzz list" << std::endl;
        std::cout << "Enter '2' to enter a number manually" << std::endl;
        std::cout << "Enter '314' to exit " << std::endl;
        std::cin >> input;

        buzzer.set_iExecutionType(input);

        int executionType = buzzer.get_iExecutionType();

        switch (executionType)
        {

        case 1:
            buzzer.executeAutomated();
            break;

        case 2:
            buzzer.executeManually();
            break;

        case 314:
            return 0;

        default:
            buzzer.printError("Execution type");
            break;
        }
       std::cout << std::endl;
    }
    return 0;
}
