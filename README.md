*FizzBuzz*

This simple app is able to print numbers 1-100.
It does that while replacing multiples of 3 by 'Fizz' and multiples of 5 by 'Buzz'.
Multiples of both (15) will be replaced by FizzBuzz.

The user can choose a manual mode or auto mode.
In manual mode, the user can enter a number himself.

In auto mode, the app will print the numbers 1-100.