#include "fizzbuzz.h"

FizzBuzz::FizzBuzz()
{
    ;
}

FizzBuzz::~FizzBuzz(void)
{
    ;
}

//PRINTS
void FizzBuzz::printFizz()
{
    std::cout << "Fizz" << std::endl;
}

void FizzBuzz::printBuzz()
{
    std::cout << "Buzz" << std::endl;
}

void FizzBuzz::printFizzBuzz()
{
    std::cout << "FizzBuzz" << std::endl;
}

void FizzBuzz::printInt()
{
    std::cout << i << std::endl;
}

void FizzBuzz::printLogic()
{
    switch (iDivisorType)
    {
    case 15:
        printFizzBuzz();
        break;
    case 5:
        printBuzz();
        break;
    case 3:
        printFizz();
        break;
    case 0:
        printInt();
        break;
    default:
        printError("Number");
        break;
    }
}

void FizzBuzz::printError(string errorType)
{
    std::cout << "ERROR:" << errorType << " could not be determined" << std::endl;
}

//CHECKS
void FizzBuzz::checkForThreeDivisor()
{
    if (i % 3 == 0)
    {
        iDivisorType = 3;
    }
}

void FizzBuzz::checkForFiveDivisor()
{
    if (i % 5 == 0)
    {
        iDivisorType = 5;
    }
}

void FizzBuzz::checkForFifteenDivisor()
{
    if (i % 5 == 0 && i % 3 == 0)
    {
        iDivisorType = 15;
    }
}

void FizzBuzz::checkEverything()
{
    checkForThreeDivisor();
    checkForFiveDivisor();
    checkForFifteenDivisor();
    isJustNumber();
}

void FizzBuzz::isJustNumber()
{
    if (i % 5 != 0 && i % 3 != 0)
    {
        iDivisorType = 0;
    }
}

//EXECUTIONS
void FizzBuzz::executeAutomated()
{
    FizzBuzz printer;
    std::cout << "You chose auto mode, so here's the list: " << std::endl;
    for (i = 1; i < 100; i++)
    {
        checkEverything();
        printLogic();
    }
}

void FizzBuzz::executeManually()
{
    FizzBuzz printer;
    std::cout << "You chose manual mode, so enter a number here: ";
    std::cin >> i;

    checkEverything();
    printLogic();
}
